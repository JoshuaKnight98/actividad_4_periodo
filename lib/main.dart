import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'details.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: Init.instance.initialize(),
      builder: (context, AsyncSnapshot snapshot) {
        // Show splash screen while waiting for app resources to load:
        if (snapshot.connectionState == ConnectionState.waiting) {
          return const MaterialApp(home: Splash());
        } else {
          // Loading is done, return the app:
          return MaterialApp(
            title: 'Flutter Demo',
            theme: ThemeData(
              // This is the theme of your application.
              //
              // Try running your application with "flutter run". You'll see the
              // application has a blue toolbar. Then, without quitting the app, try
              // changing the primarySwatch below to Colors.green and then invoke
              // "hot reload" (press "r" in the console where you ran "flutter run",
              // or simply save your changes to "hot reload" in a Flutter IDE).
              // Notice that the counter didn't reset back to zero; the application
              // is not restarted.
              primarySwatch: Colors.blue,
            ),
            home: Home(),
          );
        }
      },
    );
  }
}

class Home extends StatefulWidget {
  Home({Key? key}) : super(key: key);

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Fast Food'),
        backgroundColor: Colors.red,
      ),
      body: ListView(children: <Widget>[
        box(
            context,
            "https://image.shutterstock.com/image-photo/cheeseburger-fries-cola-on-white-260nw-1348977590.jpg",
            "Combo de Super Hamburguesa",
            "5.45",
            "Combo de super hamburguesa de doble carne y doble queso, con papas francesas, acompañados con una bebida de tu eleccion de 50ml"),
        box(
            context,
            "https://mercadito.online/wp-content/uploads/2020/12/pollo-a-la-brasa-en-los-olivos-2019-menu1-1.png",
            "Combo de Pollo Astral",
            "4.75",
            "Pollo Astral 2 piezas de pollo de tu eleccion, mas papitas francesas y ensalada con verduras fresca."),
        box(
            context,
            "https://www.tunicaragua.com/images/stories/virtuemart/product/Combo_Pizza_Hawa_565747d0ac3c7.jpg",
            "Combo de Pizza Familiar",
            "6.25",
            "Pizza Familiar de 1 ingrediente de tu eleccion: Carne, queso, hawaiiana. Mas soda de 1.5 lt de tu eleccion.")
      ]),
    );
  }

  Widget box(BuildContext context, String image, String name, String price,
      String details) {
    return Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height * 0.3,
        color: Colors.yellowAccent,
        child: GestureDetector(
          onTap: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => Details(
                  image: image,
                  name: name,
                  price: price,
                  details: details,
                ),
              ),
            );
          },
          child: Card(
            color: Colors.red[100],
            child: Column(
              children: <Widget>[
                Image.network(
                  image,
                  width: MediaQuery.of(context).size.width * 0.6,
                  height: MediaQuery.of(context).size.height * 0.2,
                ),
                Column(
                  children: <Widget>[
                    Text(
                      name,
                      style: const TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    Text(
                      price,
                      style: const TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ));
  }
}

class Splash extends StatelessWidget {
  const Splash({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    bool lightMode =
        MediaQuery.of(context).platformBrightness == Brightness.light;
    return Scaffold(
        backgroundColor: Colors.redAccent,
        body: Center(
            child:
                Column(mainAxisAlignment: MainAxisAlignment.center, children: [
          Image.network(
              "https://images.vexels.com/media/users/3/224171/isolated/preview/63b97bda25a392131e350039de130cc5-logotipo-de-sarten-en-llamas.png"),
          const Text("25-2880-2017",
              style: TextStyle(
                fontSize: 20,
                fontWeight: FontWeight.bold,
                color: Colors.black,
              ))
        ])));
  }
}

class Init {
  Init._();
  static final instance = Init._();

  Future initialize() async {
    // This is where you can initialize the resources needed by your app while
    // the splash screen is displayed.  Remove the following example because
    // delaying the user experience is a bad design practice!
    await Future.delayed(const Duration(seconds: 3));
  }
}
