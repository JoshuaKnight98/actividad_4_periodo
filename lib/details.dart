// ignore_for_file: file_names

import 'package:flutter/material.dart';

class Details extends StatefulWidget {
  String image, name, price, details;

  Details(
      {required this.image,
      required this.name,
      required this.price,
      required this.details});

  @override
  State<Details> createState() => _DetailsState();
}

class _DetailsState extends State<Details> {
  @override
  Widget build(BuildContext context) {
    return Container(
        color: Colors.white,
        child: Column(
          children: <Widget>[
            Container(
                height: MediaQuery.of(context).size.height * 0.3,
                width: MediaQuery.of(context).size.width,
                child: Image.network(widget.image, fit: BoxFit.cover)),
            Text(
              widget.name,
              style: const TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 40,
                  color: Colors.purple),
              textAlign: TextAlign.center,
            ),
            const SizedBox(
              height: 20,
            ),
            Text(
              widget.price,
              style: const TextStyle(fontSize: 25),
            ),
            const SizedBox(
              height: 20,
            ),
            Text(
              widget.details,
              style: const TextStyle(fontSize: 15, color: Colors.black),
            ),
          ],
        ));
  }
}
